/**
 * 
 */
package com.paic.arch.interviews;

/**
 * a graphic time converter could convert string time to a special graphic style
 * 
 * @author zwustudy
 * @since 1.0.0
 */
public class GraphicTimeConverter implements TimeConverter {
	
	private static final String LIGHT_NO = "Y";
	
	private static final String LIGHT_OFF = "O";
	
	private static final String SPECIAL_LIGHT_NO = "R";
	
	private static final int SECONDS_MIN = 0;
	
	private static final int SECONDS_MAX = 59;
	
	private static final int MINUTES_MIN = 0;
	
	private static final int MINUTES_MAX = 59;
	
	private static final int HOURS_MIN = 0;
	
	private static final int HOURS_MAX = 24;
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	@Override
	public String convertTime(String aTime) {
		
		String[] parts = aTime.split(":");
		if (parts.length != 3) {
			throw new IllegalArgumentException("input aTime:" + aTime + " is Illegal.");
		}
		StringBuilder builder = new StringBuilder();
		//convert seconds
		builder.append(convertSeconds(parts[2])).append(LINE_SEPARATOR);
		//convert hours
		builder.append(convertHours(parts[0])).append(LINE_SEPARATOR);
		//convert minutes
		builder.append(convertMinutes(parts[1]));
		return builder.toString();
	}
	
	private String convertSeconds(String seconds) {
		
		TimeProcessor stp = a -> {
			return Integer.parseInt(a) % 2 == 0 ? LIGHT_NO : LIGHT_OFF;
		};
		if (stp.illegalTime(seconds, SECONDS_MIN, SECONDS_MAX)) {
			throw new IllegalArgumentException("input seconds:" + seconds + " is Illegal.");
		} 
		return stp.process(seconds);
	}
	
	private String convertMinutes(String minutes) {
		
		TimeProcessor mtp = a -> {
			int _minutes = Integer.parseInt(a);
			int quotient = _minutes / 5;
			int remainder = _minutes % 5;
			StringBuilder builder = new StringBuilder();
			for (int index = 0; index < 11; index++) {
				builder.append(index < quotient ? (index + 1) % 3 == 0 ? SPECIAL_LIGHT_NO : LIGHT_NO : LIGHT_OFF);
			}
			builder.append(LINE_SEPARATOR);
			for (int index = 0; index < 4; index++) {
				builder.append(index < remainder ? LIGHT_NO : LIGHT_OFF);
			}
			return builder.toString();
		};
		if (mtp.illegalTime(minutes, MINUTES_MIN, MINUTES_MAX)) {
			throw new IllegalArgumentException("input:" + minutes + " is Illegal.");
		} 
		return mtp.process(minutes);
	}
	
	private String convertHours(String hours) {
		
		TimeProcessor htp = a -> {
			int _hours = Integer.parseInt(hours);
			int quotient = _hours / 5;
			int remainder = _hours % 5;
			StringBuilder builder = new StringBuilder();
			for (int index = 0; index < 4; index++) {
				builder.append(index < quotient ? SPECIAL_LIGHT_NO : LIGHT_OFF);
			}
			builder.append(LINE_SEPARATOR);
			for (int index = 0; index < 4; index++) {
				builder.append(index < remainder ? SPECIAL_LIGHT_NO : LIGHT_OFF);
			}
			return builder.toString();
		};
		if (htp.illegalTime(hours, HOURS_MIN, HOURS_MAX)) {
			throw new IllegalArgumentException("input:" + hours + " is Illegal.");
		} 
		return htp.process(hours);
	}
}
