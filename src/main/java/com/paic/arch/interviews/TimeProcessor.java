/**
 * 
 */
package com.paic.arch.interviews;

/**
 * 
 * a functional interface to process string time property to the special tag
 * 
 * @author zwustudy
 * @since 1.0.0
 */
@FunctionalInterface
public interface TimeProcessor {

	/**
	 * process string time property to the special tag
	 * @param time string
	 * @return special tag time
	 */
	String process(String time);

	/**
	 * check the input string time property is illegal or not
	 * @param time
	 * @param min
	 * @param max
	 * @return
	 */
	default boolean illegalTime(String time, int min, int max) {
		int _time = -1;
		try {
			_time = Integer.parseInt(time);
		} catch (NumberFormatException e) {
			return true;
		}
		if (_time < min || _time > max) {
			return true;
		}
		return false;
	}

}
